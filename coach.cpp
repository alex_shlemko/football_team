#include "coach.h"

Coach::Coach() {
    experience = 0;
    schemes = {};
}

Coach::Coach(QString firstNameC, QString lastNameC, int ageC, int heightC, int experienceC, const vector<QString>* schemesC):Person(firstNameC, lastNameC, ageC, heightC)
{
    if (experienceC > 0) experience = experienceC;
    schemes = *schemesC;
}


QString Coach::getFirstName() const{
    return firstName;
}
