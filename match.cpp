#include "match.h"





using namespace std;

Match::Match(){
    team1 = nullptr;
    team2 = nullptr;
}
Match::Match(Team* team1C, Team* team2C,const  vector<int>*statsC) {
    team1 = team1C;
    team2 = team2C;
    stats=*statsC;
}


QString Match:: getOpponents()const {
    return team1->getName() + " - " + team2->getName();
}

vector<int> Match::getStats()const{
    return stats;
}

Team* Match::getFirstTeam()const{
    return team1;
}


