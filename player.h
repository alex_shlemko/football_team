#ifndef PLAYER_H
#define PLAYER_H


#include<QString>
#include<vector>
#include"Person.h"
#pragma once

using namespace std;



class Player: public Person
{
public:
    Player();
    Player(QString firstNameC, QString lastNameC, int ageC, int heightC, QString positionC, double transferFeeC, vector<Player*>*players);




     QString getFirstName() const;

     QString getLastName() const;

     int getAge()const;

     int getHeight()const;

     QString getPosition() const;

     void setPosition(const QString&newPos);

     double getFee()const;
private:
    QString position;
    double transferFee;


};


#endif // PLAYER_H
