#include "Player.h"
#include<iostream>
#include<vector>


Player::Player(){
    position = "";
    transferFee = 0;
}

Player::Player(QString firstNameC, QString lastNameC, int ageC, int heightC, QString positionC, double transferFeeC, vector<Player*>*players):Person(firstNameC, lastNameC, ageC, heightC ) {
    position = positionC;
    transferFee = transferFeeC;
    players->push_back(this);
}

QString Player:: getFirstName() const{
    return firstName;
}

QString Player::getLastName()const {
    return lastName;
}

int Player::getAge()const{
    return age;
}

int Player::getHeight()const{
    return height;
}

QString Player::getPosition() const {
    return position;
}

void Player::setPosition(const QString&newPos){
    position=newPos;
}


double Player::getFee()const{
    return transferFee;
}
