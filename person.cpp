#include "person.h"
#include<QString>
#include<iostream>
using namespace std;



Person::Person(){
    firstName = "";
    lastName = "";
    age = 0;
    height = 0;
}

Person::Person(QString firstNameC, QString lastNameC, int ageC, int heightC) {
    firstName = firstNameC;
    lastName = lastNameC;
    age = ageC;
    height = heightC;
}



QString Person::getFullname() const{
    return firstName+" "+lastName;
}
