#include "scheme.h"

using namespace std;

Scheme::Scheme()
{
    name = "";
    type = "";
    players = {};
}

Scheme::Scheme(const string& nameC, const QString& typeC, const vector<Player*>*playersC){
    name = nameC;
    type = typeC;
    players = *playersC;
    numDefenders = nameC[0]-'0';
    numMidfielders = nameC[2] - '0';
    numForwards = nameC[4] - '0';
    for (int i = 0; i < players.size(); i++)
    {
        if (players[i]->getPosition() == "GK")goalkeepers.push_back(players[i]);
        if (players[i]->getPosition() == "FW" || players[i]->getPosition() == "RW" || players[i]->getPosition() == "LW") forwards.push_back(players[i]);
        if (players[i]->getPosition() == "CB" || players[i]->getPosition() == "RB" || players[i]->getPosition() == "LB") defenders.push_back(players[i]);
        if (players[i]->getPosition() == "CM" || players[i]->getPosition() == "RM" || players[i]->getPosition() == "LM" || players[i]->getPosition() == "AM" || players[i]->getPosition() == "DM")midfielders.push_back(players[i]);
    }
    formSquad();
}

 QString Scheme::getName()const {
     QString nameQ= QString::fromStdString(name);
    return nameQ;
}


void Scheme::formSquad() {
    bool lb=true;
    bool cb=true;
    bool rb=true;
    bool fw=true;
    int countDefs=0;
    int countAtt=0;
    squad.push_back(goalkeepers[0]);

//    for (int i = 0; i < defenders.size(); i++)
//    {


//        if(defenders[i]->getPosition()=="LB" && lb){
//             squad.push_back(defenders[i]);
//             lb=false;
//        }
//        if(defenders[i]->getPosition()=="CB"&& cb){
//            countDefs++;
//            squad.push_back(defenders[i]);
//            if(numDefenders==4){
//                if(countDefs==2)cb=false;
//            }
//            else{
//                if(countDefs==3)cb==false;
//            }

//        }
//        if(defenders[i]->getPosition()=="RB" &&rb){
//            squad.push_back(defenders[i]);
//            rb=false;
//        }

//    }

    for (int i = 0; i < defenders.size()&&countDefs<numDefenders; i++)
        {


            if (numDefenders==4 && defenders[i]->getPosition() == "LB" && lb) {
                squad.push_back(defenders[i]);
                lb=false;
                countDefs++;
            }

            if (numDefenders==4 && defenders[i]->getPosition() == "RB" && rb) {
                squad.push_back(defenders[i]);
                rb=false;
                countDefs++;
            }
            if (defenders[i]->getPosition() == "CB") {

                //                if (numDefenders == 3) {
                //                    if (countDefs == 3)cb == false;
                //                }
                //                if (numDefenders == 4) {
                //                    if (countDefs == 2)cb = false;
                //                }
                squad.push_back(defenders[i]);
                countDefs++;


            }


        }


    for (int i = 0; i < numMidfielders; i++)
    {
        squad.push_back(midfielders[i]);

    }
    for (int i = 0; i <forwards.size() &&countAtt<numForwards; i++)
        {
            if (forwards[i]->getPosition() == "RW" && numForwards == 3) {
                squad.push_back(forwards[i]);
                countAtt++;
                }
            if (forwards[i]->getPosition() == "LW" && numForwards == 3) {
                squad.push_back(forwards[i]);
                countAtt++;
            }


            if (forwards[i]->getPosition() == "FW") {
                squad.push_back(forwards[i]);
                countAtt++;
            }
        }
}


vector<Player*> Scheme::getSquad()const{
    return squad;
}









