#ifndef SCHEME_H
#define SCHEME_H


#include <QString>
#include<vector>
#include"Player.h"

using namespace std;
class Scheme
{
public:
    Scheme();
    Scheme(const string& nameC, const QString& typeC, const vector<Player*>*playersC);

     virtual QString getName() const;

      void formSquad();

      vector<Player*> getSquad()const;



private:
    string name;
    QString type;
    int numDefenders;
    int numMidfielders;
    int numForwards;
    vector<Player*>players;
    vector<Player*>squad;
    vector<Player*> goalkeepers;
    vector<Player*>defenders;
    vector<Player*>midfielders;
    vector<Player*>forwards;

};

#endif // SCHEME_H
