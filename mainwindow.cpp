#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <iostream>


MainWindow::MainWindow(Match* matchC, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    match=matchC;
//    ui->groupBox_2->hide();

    ui->ball_posession1->setTextVisible(false);
    ui->ball_posession2->setTextVisible(false);
    ui->shots1->setTextVisible(false);
    ui->shots2->setTextVisible(false);
    ui->on_target1->setTextVisible(false);
    ui->on_target2->setTextVisible(false);
    ui->missed1->setTextVisible(false);
    ui->missed2->setTextVisible(false);
    ui->freekicks1->setTextVisible(false);
    ui->freekicks2->setTextVisible(false);
    ui->corners1->setTextVisible(false);
    ui->corners2->setTextVisible(false);
    ui->offside1->setTextVisible(false);
    ui->offside2->setTextVisible(false);
    ui->saves1->setTextVisible(false);
    ui->saves2->setTextVisible(false);
    ui->fouls1->setTextVisible(false);
    ui->fouls2->setTextVisible(false);
    ui->passes1->setTextVisible(false);
    ui->passes2->setTextVisible(false);

    //posession
    int pos1=match->getStats()[0];
    int pos2=match->getStats()[1];
    ui->ball_posession1->setMaximum(pos1+pos2);
    ui->ball_posession2->setMaximum(pos1+pos2);

    ui->ball_posession1->setValue(pos1);
    ui->ball_posession2->setValue(pos2);

    ui->bps1->setText(QString::number(pos1)+"%");
    ui->bps2->setText(QString::number(pos2)+"%");

//shots
    int shots1=match->getStats()[2];
    int shots2=match->getStats()[3];

    ui->shots1->setMaximum(shots1 + shots2);
    ui->shots2->setMaximum(shots1  +shots2);

    ui->shots1->setValue(shots1);
    ui->shots2->setValue(shots2);

    ui->sh1->setText(QString::number(shots1));
    ui->sh2->setText(QString::number(shots2));

    //on target

    int on_target1=match->getStats()[4];
    int on_target2=match->getStats()[5];

   ui->on_target1->setMaximum(on_target1+on_target2);
   ui->on_target2->setMaximum(on_target1+on_target2);

   ui->on_target1->setValue(on_target1);
   ui->on_target2->setValue(on_target2);

   ui->tg1->setText(QString::number(on_target1));
   ui->tg2->setText(QString::number(on_target2));

   //missed
    int missed1=match->getStats()[6];
    int missed2=match->getStats()[7];

    ui->missed1->setMaximum(missed1+missed2);
    ui->missed2->setMaximum(missed1+missed2);

    ui->missed1->setValue(missed1);
    ui->missed2->setValue(missed2);

    ui->ms1->setText(QString::number(missed1));
    ui->ms2->setText(QString::number(missed2));

    //freekicks
    int free1=match->getStats()[10];
    int free2=match->getStats()[11];

    ui->freekicks1->setMaximum(free1+ free2);
    ui->freekicks2->setMaximum(free1+ free2);

    ui->freekicks1->setValue(free1);
    ui->freekicks2->setValue(free2);

    ui->fk1->setText(QString::number(free1));
    ui->fk2->setText(QString::number(free2));

    //corners

    int cor1=match->getStats()[12];
    int cor2=match->getStats()[13];

    ui->corners1->setMaximum(cor1+cor2);
    ui->corners2->setMaximum(cor1+cor2);

    ui->corners1->setValue(cor1);
    ui->corners2->setValue(cor2);

    ui->cor1->setText(QString::number(cor1));
    ui->cor2->setText(QString::number(cor2));

    //offsides

    int of1=match->getStats()[14];
    int of2=match->getStats()[15];

    ui->offside1->setMaximum(of1+of2);
    ui->offside2->setMaximum(of1+of2);

    ui->offside1->setValue(of1);
    ui->offside2->setValue(of2);

    ui->of1->setText(QString::number(of1));
    ui->of2->setText(QString::number(of2));

    //saves

    int save1=match->getStats()[16];
    int save2=match->getStats()[17];

    ui->saves1->setMaximum(save1+save2);
    ui->saves2->setMaximum(save1+save2);

    ui->saves1->setValue(save1);
    ui->saves2->setValue(save2);

    ui->sa1->setText(QString::number(save1));
    ui->sa2->setText(QString::number(save2));

    //fouls

    int foul1=match->getStats()[18];
    int foul2=match->getStats()[19];

    ui->fouls1->setMaximum(foul1+foul2);
    ui->fouls2->setMaximum(foul1+foul2);

    ui->fouls1->setValue(foul1);
    ui->fouls2->setValue(foul2);

    ui->fo1->setText(QString::number(foul1));
    ui->fo2->setText(QString::number(foul2));

    //passes

    int pass1=match->getStats()[20];
    int pass2=match->getStats()[21];

    ui->passes1->setMaximum(pass1+pass2);
    ui->passes2->setMaximum(pass1+pass2);

    ui->passes1->setValue(pass1);
    ui->passes2->setValue(pass2);

    ui->ps1->setText(QString::number(pass1));
    ui->ps2->setText(QString::number(pass2));

    if(match->getFirstTeam()->getScheme()->getName()=="4-3-3"){
        ui->fourFourTwo->hide();
        ui->threeFiveTwo->hide();
    }

    if(match->getFirstTeam()->getScheme()->getName()=="4-4-2"){
        ui->fourThreeThree->hide();
        ui->threeFiveTwo->hide();
    }

    if(match->getFirstTeam()->getScheme()->getName()=="3-5-2"){
        ui->fourFourTwo->hide();
        ui->fourThreeThree->hide();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}










void MainWindow::on_pushButton_3_clicked()
{
    Scheme* scheme=match->getFirstTeam()->getScheme();

QList<QLabel *> list;

if(scheme->getName()=="4-3-3"){

    list= ui->fourThreeThree -> findChildren<QLabel *> ();
}

if(scheme->getName()=="4-4-2")list=ui->fourFourTwo->findChildren<QLabel*>();

if(scheme->getName()=="3-5-2")list=ui->threeFiveTwo->findChildren<QLabel*>();
vector<QLabel*> finalList;

for(int i=0;i<list.size();i++){
   if(list[i]->objectName().contains("GK"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("RB"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("LB"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CB1"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CB2"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CB3"))finalList.push_back(list[i]);

   if(list[i]->objectName().contains("CM1"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CM2"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CM3"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CM4"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("CM5"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("FW1"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("FW2"))finalList.push_back(list[i]);

   if(list[i]->objectName().contains("LW"))finalList.push_back(list[i]);
   if(list[i]->objectName().contains("RW"))finalList.push_back(list[i]);
 if(scheme->getName()=="4-3-3") if(list[i]->objectName().contains("FW"))finalList.push_back(list[i]);
}

for(size_t i=0;i<finalList.size();i++){
    for(size_t j=0;j<scheme->getSquad().size();j++){
        if(finalList[i]->objectName().contains(scheme->getSquad()[j]->getPosition())){
            finalList[i]->setText(scheme->getSquad()[j]->getLastName());
            scheme->getSquad()[j]->setPosition("used");
            break;
    }
}
}


}



































