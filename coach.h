#ifndef COACH_H
#define COACH_H


#include "Person.h"
#include <QString>
#include <vector>

using namespace std;

class Coach : public Person
{
public:

    Coach();

    Coach(QString firstNameC, QString lastNameC, int ageC, int heightC, int experienceC, const vector<QString>* schemesC);


    QString getFirstName()const;

private:
    int experience;
    vector<QString>schemes;
    };

#endif // COACH_H
