#include "team.h"

#include "Team.h"
#include "Player.h"
#include "Coach.h"
#include<iostream>

Team::Team()
{
    name = "";
    country = "";
    coach = nullptr;
    players = {};
    scheme = nullptr;
}

Team::Team(const QString& nameC, const QString&countryC,   Coach* coachC, const vector<Player*>*playersC) {

    name = nameC;
    country = countryC;
    coach = coachC;
    players = *playersC;
}
Team::Team(const QString& nameC, const QString&countryC, Coach* coachC, const vector<Player*>*playersC, Scheme* schemeC) {

    name = nameC;
    country = countryC;
    coach = coachC;
    players = *playersC;
    scheme = schemeC;
}


QString Team:: getSchemeName()const {
   return scheme->getName();
}

QString Team::getName()const {
    return name;
}

vector<Player*> Team::getPlayers()const{
    return players;
}

QString Team::getPlayer() const{
    return players[0]->getFirstName();
}

vector<Player*> Team::formSquad(Scheme* scheme) const{
   scheme->formSquad();
   return scheme->getSquad();
}

Scheme* Team::getScheme()const{
    return scheme;
}



























