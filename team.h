#ifndef TEAM_H
#define TEAM_H

#include "Coach.h"
#include "Player.h"
#include"Scheme.h"
#include<QString>
#include <vector>


using namespace std;

class Team
{
public:
    Team();
    Team( const QString& nameC, const QString&countryC,  Coach* coachC , const vector<Player*>*playersC );
    Team(const QString& nameC, const QString&countryC, Coach* coachC, const vector<Player*>*playersC, Scheme* schemeC );


    QString getSchemeName()const;

    QString getName() const;

    QString getPlayer()const;

    vector<Player*> getPlayers() const;

    vector<Player*>formSquad(Scheme* scheme) const;

    Scheme*getScheme()const;

private:
    QString name;
    QString country;
    Coach* coach;
    vector<Player*>players;
    Scheme* scheme;

};


#endif // TEAM_H
