#include "mainwindow.h"
#include <QApplication>
#include "person.h"
#include "coach.h"
#include <vector>
#include "scheme.h"
#include<iostream>

int main(int argc, char *argv[])
{
    vector<QString>schemes_LP = {"4-3-3","4-5-1" };
    vector<Player*>players;
    vector<int>stats={50,50,16,11,4,3,3,5,9,3,12,10,7,8,0,5,2,3,10,7,1000,500,17,23};

        Player Karius("Loris", "Karius", 24, 189, "GK", 30,&players);
        Player Mignolet("Simon", "Mignolet", 30, 193, "GK", 10, &players);
        Player Ward("Danny", "Ward", 24, 191, "GK", 7, &players);

        Player Clyne("Nathaniel", "Clyne", 27, 175, "RB", 40, &players);
        Player Trent("Alexander-Arnold", "Trent", 19, 175, "RB", 50, &players);
        Player Robertson("Andrew", "Robertson", 24, 178, "LB", 20, &players);
        Player Moreno("Alberto", "Moreno", 25, 171, "LB", 25, &players);
        Player vanDijk("Virgil", "van Dijk", 26, 193, "CB", 90, &players);
        Player Lovren("Dejan", "Lovren", 28, 188, "CB", 40, &players);
        Player Matip("Joel", "Matip", 26, 195, "CB", 20, &players);
        Player Klavan("Ragnar", "Klavan", 32, 187, "CB", 15, &players);
        Player Gomez("Joe", "Gomez", 20, 188, "CB", 30, &players);



        Player Wijnaldum("Georginio", "Wijnaldum", 27, 175, "CM", 40, &players);
        Player Henderson("Jordan", "Henderson", 27, 182, "CM", 60, &players);
        Player Lallana("Adam", "Lallana", 30, 172, "CM", 40, &players);
        Player Can("Emre", "Can", 24, 184, "CM", 70, &players);
        Player Chamberlain("Alex", "Oxlade-Chemberlain", 24, 180, "CM", 50, &players);

        Player Salah("Mohammed", "Salah", 25, 175, "RW", 110, &players);
        Player Mane("Sadio", "Mane", 26, 175, "LW", 80, &players);
        Player Firmino("Roberto", "Firmino", 26, 182, "FW", 90, &players);
        Player Solanke("Dominic", "Solanke", 20, 187, "FW", 12, &players);
        Player Ings("Danny", "Ings", 25, 178, "FW", 5, &players);


        Coach Klopp("Jurgen", "Klopp", 52, 186, 17, &schemes_LP);

        Scheme fourThreeThree("4-3-3", "balance", &players);
        Scheme fourFourTwo("4-4-2", "balance", &players);
        Scheme threeFiveTwo("3-5-2","balance", &players);
        Team Liverpool("Liverpool", "GB", &Klopp, &players, &threeFiveTwo);

        Match final(&Liverpool, &Liverpool,&stats);

    QApplication a(argc, argv);
    MainWindow w(&final);

     w.show();

    return a.exec();
}
