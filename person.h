#ifndef PERSON_H
#define PERSON_H
#include<QString>
#pragma once
using namespace std;
class Person
{
public:
    Person();
    Person(QString firstNameC, QString lastNameC, int ageC, int heightC);
    QString getFullname()const;
//    void display() const ;
protected:
    QString firstName;
    QString lastName;
    int age;
    int height;
//    friend ostream& operator<<(ostream& os, const Person& ps);
};


#endif // PERSON_H
