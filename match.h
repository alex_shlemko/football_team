#ifndef MATCH_H
#define MATCH_H

#include"team.h"
class Match
{
public:
    Match();
    Match(Team* team1C, Team*team2C, const vector<int>*statsC);


    QString getOpponents()const;

    vector<int> getStats()const;

    Team* getFirstTeam()const;

private:

    Team* team1;
    Team* team2;
    vector<int>stats;

};
#endif // MATCH_H
