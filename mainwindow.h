#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>
#include "player.h"
#include "coach.h"
#include "scheme.h"
#include "team.h"
#include "match.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Match* matchC, QWidget *parent = 0);
    ~MainWindow();

    void fillPosition(const QString&schemeName)const;

private slots:




    void on_pushButton_3_clicked();



private:
    Ui::MainWindow *ui;
 Match* match;
};

#endif // MAINWINDOW_H
